﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using SimpleJSON;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using UnityEngine.SceneManagement;


public class StoryManager : MonoBehaviour {

    public Text StoryText;
    public Text[] ChoiceButtonTexts;
    public Text IntellectValue;
    public Text ForceValue;
    public Text CharismaValue;
    public GameObject[] choicebuttons;
    private JSONNode parsedquest;
    private JSONNode parsedplayersaves;
    public GameObject TextPanel;
    private string levelnumber = "0";
    private string chapter = "1";
    private int parnum = 0;
    private int choicenum = 0;
    public ScrollRect TextScrollRect;
    public bool istextprinting;
    public float printingspeed;
    public float printingtime;
    public Slider SpeedSlider;
    [SerializeField]
    private Text SizeTestText;
    private int fontsize;
    private string playerDataPath;
    private string z;

    public int force;
    public int intellect;
    public int charisma;
    public int karma;
    public int mana;
    public Dictionary<string, JsonObjects> levels;

    void Start()
    {
        playerDataPath = Application.streamingAssetsPath + "/Saves/Saves.json";
        string jsonstring = File.ReadAllText(playerDataPath);
        parsedplayersaves = JSON.Parse(jsonstring);
        //TextAsset TextQuest = Resources.Load("TextQuest") as TextAsset;
        //parsedquest = JSON.Parse(TextQuest.text);
        TextAsset TextQuest = Resources.Load("LevelInfo") as TextAsset;
        parsedquest = JSON.Parse(TextQuest.text);
        force = Int32.Parse(parsedplayersaves["Player"]["1"]["Stats"]["force"]);
        intellect = Int32.Parse(parsedplayersaves["Player"]["1"]["Stats"]["intellect"]);
        charisma = Int32.Parse(parsedplayersaves["Player"]["1"]["Stats"]["charisma"]);
        karma = Int32.Parse(parsedplayersaves["Player"]["1"]["Stats"]["karma"]);
        mana = Int32.Parse(parsedplayersaves["Player"]["1"]["Stats"]["mana"]);
    }

    public void Load()
    {
        Dictionary<string, JsonObjects> levels = GetLevels();
        editor.showstats = true;
        GameObject msg = editor.msg;
        GameObject nxt_lvl = editor.nxt_lvl;
        List<GameObject> player_images = editor.player_images;
        List<GameObject> variants = editor.variants;
        string msg_txt = "";
        string num_nxt_lvl = "";
        var cur_level_num = level_select.GetComponent<Dropdown>().captionText.text;
        if (cur_level_num.Length != 0)
        {
            var cur_level = levels[cur_level_num];
            msg_txt = cur_level.msg;
            if (msg_txt.Length != 0)
            {
                if (!msg.activeInHierarchy)
                    msg.SetActive(true);
            }
            num_nxt_lvl = cur_level.nextlevel;
            msg.GetComponent<InputField>().text = msg_txt;
            nxt_lvl.GetComponent<InputField>().text = num_nxt_lvl;

            LoadLvlStat(cur_level.force, stats.st_force, stats.sign_force);
            LoadLvlStat(cur_level.intellect, stats.st_intellect, stats.sign_intellect);
            LoadLvlStat(cur_level.charisma, stats.st_charisma, stats.sign_charisma);
            LoadLvlStat(cur_level.karma, stats.st_karma, stats.sign_karma);
            LoadLvlStat(cur_level.mana, stats.st_mana, stats.sign_mana);

            string img_path = cur_level.img_path;
            editor.img_path = LoadImg(img_path, editor.background, editor.bck_path);

            if (editor.count_player_images > 0)
            {
                var item = player_images[0];
                player_images[0] = player_images[player_images.Count - 1];
                player_images[player_images.Count - 1] = item;
                player_images[0].transform.position = item.transform.position;
            }
            while (editor.count_player_images > 0)
            {
                Destroy(player_images[player_images.Count - 1]);
                player_images.RemoveAt(player_images.Count - 1);
                editor.count_player_images--;
            }

            if (cur_level.player_images.Length > 0)
            {
                string[] array_images = Regex.Split(cur_level.player_images, Regex.Escape("||"));
                for (int i = 0; i < array_images.Length; i++)
                {
                    editor.CreatePlayerImages(player_images[i]);
                    player_images[i].GetComponent<PlayerImage>().img_path =
                        LoadImg(array_images[i], player_images[i].GetComponent<Image>(), editor.p_img_path);
                }
            }
            while (editor.variants.Count != 0)
            {
                Destroy(variants[variants.Count - 1]);
                variants.RemoveAt(variants.Count - 1);
            }
            if (cur_level.variants.Length > 0)
            {
                string[] array_variants = Regex.Split(cur_level.variants, Regex.Escape("||"));
                for (int i = 0; i < array_variants.Length; i++)
                {
                    editor.AddVariant();
                    var array_stats = array_variants[i].Split('_');
                    editor.variants[i].GetComponentInChildren<InputField>().text = array_stats[0];


                    var v_st = editor.variants[i].GetComponentInChildren<Stats>(true);

                    if (array_stats.Length >= 2)
                    {
                        for (int j = 1; j < array_stats.Length; j++)
                        {
                            var array_nxt_lvl = array_stats[j].Split(':');
                            if (j == array_stats.Length - 1)
                            {
                                editor.variants[i].GetComponent<Variant>().nxt_lvl.text = array_nxt_lvl[1];
                            }
                            else
                            {
                                SetVrntStatValue(array_nxt_lvl, v_st);
                            }
                        }
                    }
                }
            }
            editor.level_num.GetComponent<InputField>().text = cur_level_num;
        }
    }

    public Dictionary<string, JsonObjects> GetLevels()
    {
        Dictionary<string, JsonObjects> levels = new Dictionary<string, JsonObjects>();

        if (File.Exists(json_path))
        {
            string[] lines = File.ReadAllLines(json_path);
            if (lines.Length != 0)
            {
                foreach (var str in lines)
                {
                    JsonObjects obj = JsonUtility.FromJson<JsonObjects>(str);
                    var level_num = obj.level_num;
                    if (!levels.ContainsKey(level_num))
                    {
                        levels.Add(level_num, obj);
                    }
                }
            }
        }
        return levels;
    }
    void Update()
    {
        IntellectValue.text = Convert.ToString(intellect);
        ForceValue.text = Convert.ToString(force);
        CharismaValue.text = Convert.ToString(charisma);
        if (TextPanel.activeSelf == false && Input.GetMouseButtonDown(0))
        {
            TextPanel.gameObject.SetActive(true);
        }
        fontsize = SizeTestText.cachedTextGenerator.fontSizeUsedForBestFit;
        StoryText.fontSize = fontsize;
        foreach (Text text in ChoiceButtonTexts)
        {
            text.fontSize = fontsize;
        } 
    }

    public void SpeedSliderAction()
    {
        printingtime = 1 / Mathf.Exp(SpeedSlider.value);
    }

    public void PrintingText()
    {
        z = parsedquest["Levels"][levelnumber][chapter][parnum];
    }
    //public IEnumerator PrintingText()
    //{
    //    istextprinting = true;
    //    while (parsedquest["Levels"][levelnumber][chapter][parnum] != null)
    //    {
    //        SpeedSliderAction();
    //        int i = 0;
    //        z = parsedquest["Levels"][levelnumber][chapter][parnum];
    //        ChangeStats();
    //        while (i < z.Length)
    //        {
    //            StoryText.text = StoryText.text + z[i];
    //            i++;
    //            TextScrollRect.verticalNormalizedPosition = 0;
    //            yield return new WaitForSeconds(printingtime);
    //        }
    //        if (parsedquest["Levels"][levelnumber][chapter][parnum] != null)
    //        {
    //            StoryText.text = StoryText.text + '\n' + '\n';
    //        }
    //        parnum++;
    //    }
    //    istextprinting = false;
    //    while (parsedquest["Choices"][levelnumber][chapter][choicenum] != null)
    //    {
    //        z = parsedquest["Choices"][levelnumber][chapter][choicenum];
    //        choicebuttons[choicenum].gameObject.SetActive(true);
    //        ChoiceButtonTexts[choicenum].text = Regex.Replace(z, @"(\<CHAPTER>)([^=;]*)(</CHAPTER>)", String.Empty);
    //        choicenum++;
    //    }
    //}

    public void MenuButtonAction()
    {
        SceneManager.LoadScene("Menu");
    }

    void ChangeStats()
    {
        z = parsedquest["Levels"][levelnumber][chapter][parnum];
        string stat = Regex.Match(z, @"\<(\w+?)\>").Groups[1].Value;
        string changes;
        switch (stat)
        {
            case "CHARISMA":               
                changes = Regex.Match(z, @"(\<CHARISMA>)([^=;]*)(</CHARISMA>)").Groups[2].Value;
                switch (changes[0])
                {
                    case '+':
                        karma = karma + Convert.ToInt32(Char.GetNumericValue(changes[1]));
                        break;
                    case '-':
                        karma = karma - Convert.ToInt32(Char.GetNumericValue(changes[1]));
                        break;
                }
                break;
            case "INTELLECT":
                changes = Regex.Match(z, @"(\<INTELLECT>)([^=;]*)(</INTELLECT>)").Groups[2].Value;
                switch (changes[0])
                {
                    case '+':
                        intellect = intellect + Convert.ToInt32(Char.GetNumericValue(changes[1]));
                        break;
                    case '-':
                        intellect = intellect - Convert.ToInt32(Char.GetNumericValue(changes[1]));
                        break;
                }
                break;
            case "FORCE":
                changes = Regex.Match(z, @"(\<FORCE>)([^=;]*)(</FORCE>)").Groups[2].Value;
                switch (changes[0])
                {
                    case '+':
                        force = force + Convert.ToInt32(Char.GetNumericValue(changes[1]));
                        break;
                    case '-':
                        force = force - Convert.ToInt32(Char.GetNumericValue(changes[1]));
                        break;
                }
                break;
            case "KARMA":
                changes = Regex.Match(z, @"(\<KARMA>)([^=;]*)(</KARMA>)").Groups[2].Value;
                switch (changes[0])
                {
                    case '+':
                        karma = karma + Convert.ToInt32(Char.GetNumericValue(changes[1]));
                        break;
                    case '-':
                        karma = karma - Convert.ToInt32(Char.GetNumericValue(changes[1]));
                        break;
                }
                break;
        }
        z = Regex.Replace(z, @"(\<[^=;]*>)", String.Empty);
    }

    public void ChoiceButtonAction(int n)
    {
        z = parsedquest["Choices"][levelnumber][chapter][n];       
        chapter = Regex.Match(z, @"(\<CHAPTER>)([^=;]*)(</CHAPTER>)").Groups[2].Value;
        parnum = 0;
        choicenum = 0;
        StoryText.text = null;        
        foreach (GameObject buttons in choicebuttons)
        {
            buttons.gameObject.SetActive(false);
        }
        TextPanel.gameObject.SetActive(false);
        /*
        switch (chapter)
        {
            case "6":
                if (charisma > 3)
                    chapter = "6.1";
                else
                    chapter = "6.2";
                break;
        }
        */
    }
}
