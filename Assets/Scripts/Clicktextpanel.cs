﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Clicktextpanel : MonoBehaviour, IPointerClickHandler
{
    public Camera mainCamera;
    public StoryManager storymanager;
    // Update is called once per frame
    public void OnPointerClick(PointerEventData eventData)
    {
        if (storymanager.istextprinting)
        {
            storymanager.printingtime = 0;
        }
        else
        {
            StartCoroutine(storymanager.PrintingText());
        }
    }
}
