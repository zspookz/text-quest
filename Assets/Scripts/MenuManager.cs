﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using SimpleJSON;
using System.Collections;
using System.Text.RegularExpressions;
using System;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public Text IntellectValue;
    public Text ForceValue;
    public Text CharismaValue;
    public Text SkillPoints;
    private JSONNode parsedplayersaves;

    public int force;
    public int intellect;
    public int charisma;
    public int karma;
    public int mana;
    public int skillpoints;
    private string playerDataPath;

    void Start()
    {
        playerDataPath = Application.streamingAssetsPath + "/Saves/Saves.json";
        if (!File.Exists(playerDataPath))
        {
            TextAsset Saves = Resources.Load("PlayerSavesDummy") as TextAsset;
            parsedplayersaves = JSON.Parse(Saves.text);
            Debug.Log(parsedplayersaves);
            File.Create(playerDataPath).Dispose();
            Save();
        }
        else
        {
            string jsonstring = File.ReadAllText(playerDataPath);
            parsedplayersaves = JSON.Parse(jsonstring);

            force = Int32.Parse(parsedplayersaves["Player"]["1"]["Stats"]["force"]);
            charisma = Int32.Parse(parsedplayersaves["Player"]["1"]["Stats"]["charisma"]);
            intellect = Int32.Parse(parsedplayersaves["Player"]["1"]["Stats"]["intellect"]);
        }
        Debug.Log(parsedplayersaves);
        skillpoints = 7 - (force+charisma+intellect);
    }

    public void PlusButonAction(string stat)
    {
        switch (stat)
        {

            case "force":
                if (force < 4 && skillpoints > 0)
                {
                    force++;
                    skillpoints--;
                    parsedplayersaves["Player"]["1"]["Stats"]["force"] = Convert.ToString(force);
                }
                break;
            case "charisma":
                if (charisma < 4 && skillpoints > 0)
                {
                    charisma++;
                    skillpoints--;
                    parsedplayersaves["Player"]["1"]["Stats"]["charisma"] = Convert.ToString(charisma);
                }
                break;
            case "intellect":
                if (intellect < 4 && skillpoints > 0)
                {
                    intellect++;
                    skillpoints--;
                    parsedplayersaves["Player"]["1"]["Stats"]["intellect"] = Convert.ToString(intellect);
                }
                break;
        }
    }

    public void MinusButonAction(string stat)
    {
        switch (stat)
        {
            case "force":
                if (force > 0)
                {
                    force--;
                    skillpoints++;
                    parsedplayersaves["Player"]["1"]["Stats"]["force"] = Convert.ToString(force);
                }
                break;
            case "charisma":
                if (charisma > 0)
                {
                    charisma--;
                    skillpoints++;
                    parsedplayersaves["Player"]["1"]["Stats"]["charisma"] = Convert.ToString(charisma);
                }
                break;
            case "intellect":
                if (intellect > 0)
                {
                    intellect--;
                    skillpoints++;
                    parsedplayersaves["Player"]["1"]["Stats"]["intellect"] = Convert.ToString(intellect);

                }
                break;
        }
    }

    public void StartButtonAction()
    {
        if (skillpoints == 0)
        {
            Save();
            SceneManager.LoadScene("Level0");
        }
    }
    void Save()
    {
        string text = parsedplayersaves;
        Debug.Log(text);
        File.WriteAllText(playerDataPath, parsedplayersaves.ToString());
    }

    void Update()
    {
        IntellectValue.text = Convert.ToString(intellect);
        ForceValue.text = Convert.ToString(force);
        CharismaValue.text = Convert.ToString(charisma);
        SkillPoints.text = Convert.ToString(skillpoints);
    }
}
