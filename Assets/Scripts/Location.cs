﻿using UnityEngine;
using System.Collections;

public class Location : MonoBehaviour {

    void Scroll () {
        RectTransform rt = this.GetComponent<RectTransform>();
        this.transform.position = new Vector3(transform.position.x, rt.sizeDelta.y / 2, transform.position.z);
    }
}
